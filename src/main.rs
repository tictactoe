extern crate getopts;

use getopts::Options;
use std::fmt;
use std::env;

#[derive(Copy, Clone, PartialEq)]
enum Sign {
    None,
    Cross,
    Circle,
}

#[derive(Copy, Clone)]
struct Game {
    grid: [[Sign; 3]; 3],
    next: Sign,
}

struct Tree {
    id: usize,
    content: Content,
}

struct Node {
    game: Game,
    rest: Vec<Tree>,
}

enum Content {
    Node(Node),
    Leaf(Sign),
}

impl fmt::Display for Sign {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match *self {
            Sign::None => '_',
            Sign::Cross => 'X',
            Sign::Circle => 'O',
        })
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.grid.len() {
            for j in 0..self.grid[i].len() {
                if let Err(e) = write!(f, "{}", self.grid[i][j]) {
                    return Err(e);
                }
            }
            if i < (self.grid.len() - 1) {
                if let Err(e) = write!(f, "\\n") {
                    return Err(e);
                }
            }
        }
        Ok(())
    }
}

impl fmt::Display for Content {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Content::Node(ref n) => write!(f, "[label=\"{}\"]", n.game),
            &Content::Leaf(s) => write!(f, "[label=\"{}\"]", s),
        }
    }
}

impl fmt::Display for Tree {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Err(e) = write!(f, "    {}{};\n",
                               self.id, self.content) {
            return Err(e);
        }
        match self.content {
            Content::Node(ref n) => {
                for t in &n.rest {
                    if let Err(e) = write!(f, "{}    {} -> {};\n", t, self.id, t.id) {
                        return Err(e);
                    }
                }
            },
            Content::Leaf(_) => {},
        }
        Ok(())
    }
}

impl Game {
    fn new() -> Game {
        Game{
            grid: [[Sign::None; 3]; 3],
            next: Sign::Cross,
        }
    }

    fn get_free_compartments(&self) -> Vec<(usize, usize)> {
        (0..self.grid.len()).flat_map(|x|
            (0..self.grid[0].len()).filter_map(|y|
                if self.grid[x][y] == Sign::None {
                    Some((x, y))
                } else {
                    None
                }
            ).collect::<Vec<_>>()
        ).collect()
    }

    #[inline]
    fn is_full(&self) -> bool {
        self.get_free_compartments().len() == 0
    }

    fn play(&self, (x, y): (usize, usize)) -> Game {
        Game {
            grid: {
                let mut grid = self.grid.clone();
                grid[x][y] = self.next;
                grid
            },
            next: match self.next {
                Sign::None => Sign::None,
                Sign::Cross => Sign::Circle,
                Sign::Circle => Sign::Cross,
            },
        }
    }

    fn is_over(&self) -> Option<Sign> {
        for &s in &[Sign::Cross, Sign::Circle] {
            for i in 0..self.grid.len() {
                let mut row = true;
                for j in 0..self.grid[i].len() {
                    if self.grid[i][j] != s {
                        row = false;
                        break;
                    }
                }
                if row {
                    return Some(s);
                }
            }
            for i in 0..self.grid[0].len() {
                let mut col = true;
                for j in 0..self.grid.len() {
                    if self.grid[j][i] != s {
                        col = false;
                        break;
                    }
                }
                if col {
                    return Some(s);
                }
            }
            {
                let mut diag0 = true;
                let mut diag1 = true;
                for i in 0..self.grid.len() {
                    if self.grid[i][i] != s {
                        diag0 = false;
                    }
                    if self.grid[(self.grid.len() - 1) - i][i] != s {
                        diag1 = false;
                    }
                }
                if diag0 || diag1 {
                    return Some(s);
                }
            }
        }
        if self.is_full() {
            return Some(Sign::None);
        }
        None
    }
}

impl Tree {
    fn new(g: Game, id: &mut usize, max_id: usize, depth: usize, max_depth: usize) -> Tree {
        Tree {
            id: *id,
            content: Content::Node(Node {
                game: g,
                rest: if let Some(s) = g.is_over() {
                    *id += 1;
                    vec![Tree {
                        id: *id,
                        content: Content::Leaf(s),
                    }]
                } else if depth < max_depth {
                    g.get_free_compartments().iter().filter_map(|&c| {
                        *id += 1;
                        if *id < max_id {
                            Some(Tree::new(g.play(c), id, max_id, depth + 1, max_depth))
                        } else {
                            None
                        }
                    }).collect()
                } else {
                    Vec::new()
                }
            }),
        }
    }

    fn only_winning(self, sign: Sign) -> Option<Tree> {
        match self.content {
            Content::Node(Node { game: g, rest: r }) => {
                let mut rest = Vec::new();/*r.iter().filter_map(|t|
                    t.only_winning(sign)
                ).collect::<Vec<Tree>>();*/
                for t in r {
                    if let Some(wt) = t.only_winning(sign) {
                        rest.push(wt);
                    }
                }
                if rest.len() > 0 {
                    Some(Tree {
                        id: self.id,
                        content: Content::Node(Node {
                            game: g,
                            rest: rest,
                        }),
                    })
                } else {
                    None
                }
            },
            Content::Leaf(s) => if s == sign {
                Some(self)
            } else {
                None
            },
        }
    }
}


fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
    let default_max_id = std::usize::MAX;
    let default_max_depth = std::usize::MAX;

    let args: Vec<String> = env::args().collect();
    let ref program = args[0];
    
    let mut opts = Options::new();
    opts.optopt("d", "depth", "maximum depth search", "INTEGER");
    opts.optopt("w", "winner", "game winner", "(X, O, _, ?)");
    opts.optopt("n", "nodes", "maximum nodes number", "INTEGER");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };

    let max_id = if let Some(n) = matches.opt_str("n") {
        match n.parse() {
            Ok(v) => v,
            Err(_) => default_max_id,
        }
    } else {
        default_max_id
    };

    let max_depth = if let Some(d) = matches.opt_str("d") {
        match d.parse() {
            Ok(v) => v,
            Err(_) => default_max_depth,
        }
    } else {
        default_max_depth
    };

    let ws = if let Some(w) = matches.opt_str("w") {
        match w.as_ref() {
            "X" => Some(Sign::Cross),
            "x" => Some(Sign::Cross),
            "O" => Some(Sign::Circle),
            "o" => Some(Sign::Circle),
            "_" => Some(Sign::None),
            v => panic!("winner option expects arguement X, x, O, o or _, but \"{}\" found", v),
        }
    } else {
        None
    };

    if !matches.free.is_empty() {
        print_usage(&program, opts);
        return
    } else {
        let t = Tree::new(Game::new(), &mut 0, max_id, 0, max_depth);
        let wt = match ws {
            Some(s) => t.only_winning(s),
            None => Some(t),
        };
        match wt {
            Some(tree) => {
                println!("digraph G {{");
                println!("    graph[fontname=\"Courier New\"];");
                println!("    node[fontname=\"Courier New\"];");
                println!("    edge[fontname=\"Courier New\"];");
                print!("{}", tree);
                println!("}}");
            },
            None => println!("no result"),
        }
    }
}
